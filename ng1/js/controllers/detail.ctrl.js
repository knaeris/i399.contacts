(function () {
    'use strict';

    angular.module('app').controller('DetailController', Ctrl);

    function Ctrl($http, $routeParams, $location) {
        var vm = this
        vm.item = {};
        var itemId = $routeParams.id;
        this.editItem = editItem;

        $http.get('api/contacts/' + itemId).then(function (result) {
            vm.item = result.data;
        });


        function editItem() {

            var item = {

                name: vm.item.name,
                phone: vm.item.phone


            };
            return $http.put('api/contacts/' + itemId, vm.item).then(function () {
                $location.path('/Search');
            });


        }


    }
})();
