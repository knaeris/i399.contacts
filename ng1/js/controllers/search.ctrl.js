(function () {
    'use strict';

    angular.module('app').controller('SearchController', Ctrl);

    function Ctrl($http, modalService){
        var vm = this;
        this.items = [];
        this.ids = [];
        this.srchStr = '';
        this.removeItem = removeItem;
        this.deleteMultiple = deleteMultiple;
        this.find = find;
        this.selectedItems = selectedItems;

        init();

        function init() {
            $http.get('api/contacts').then(function (result) {
                vm.items = result.data;
                console.log(vm.items)
            });
        }
        function removeItem(id){
            modalService.confirm().then(function() {
                return $http.delete('api/contacts/' + id);}).then(init);

        }
        function find(item) {
            if (!vm.srchStr || (item.name.toLowerCase().indexOf(vm.srchStr) != -1) ||
                (item.phone.toLowerCase().indexOf(vm.srchStr.toLowerCase()) != -1) ){
                return true;
            }
            return false;
        };


        function selectedItems(id, selected) {


            if(selected == true) {
                vm.ids.push(id);
            }
            if(selected == false){
                vm.ids.pop(id);
            }

        }
        function deleteMultiple(){

            $http.post('api/contacts/delete', vm.ids).then(init);



        }

    }

})();
