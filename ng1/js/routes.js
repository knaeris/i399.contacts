(function () {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {

        $routeProvider.when('/new', {
            templateUrl : 'views/new.html',
            controller : 'NewController',
            controllerAs : 'vm'
        }).when('/search', {
            templateUrl : 'views/search.html',
            controller : 'SearchController',
            controllerAs : 'vm'
        }).when('/details/:id', {
            templateUrl : 'views/details.html',
            controller : 'DetailController',
            controllerAs : 'vm'
        }).otherwise('/search');

    }

})();