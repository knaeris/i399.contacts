'use strict';

const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;
const COLLECTION = 'contact-collection';

class Dao{

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    findAll() {
        return this.db.collection(COLLECTION).find().toArray();
    }
    insert(data){
        return this.db.collection(COLLECTION).insertOne(data)
    }
    update(id, data){
        id = new ObjectID(id);
        data._id=id;
        return this.db.collection(COLLECTION).findOneAndUpdate({_id:id}, data);
    }




    findById(id) {
        id = new ObjectID(id);
        return this.db.collection(COLLECTION).findOne({_id:id});
    }

    remove(id){
        id = new ObjectID(id);
        return this.db.collection(COLLECTION).remove({_id:id});
    }
    removeMultiple(ids){

        ids = ids.map(id => new ObjectID(id));
        console.log(ids);

        return this.db.collection(COLLECTION).remove({_id: {$in: ids}}, false);
    }

    close() {
        if (this.db) {
            this.db.close();
        }
    }
}
module.exports = Dao;