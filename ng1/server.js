'use strict';
const mongodb = require('mongodb');
const express = require('express');
const Dao = require('./dao.js');
const ObjectID = mongodb.ObjectID;
const bodyParser = require('body-parser');
const app = express();
var url = 'mongodb://user1:123456@ds263367.mlab.com:63367/i399';



app.use(bodyParser.json()); // add body parser


app.get('/api/contacts', getAllItems);
app.get('/api/contacts/:id', getItem);
app.post('/api/contacts', addItem);
app.put('/api/contacts/:id', updateItem);
app.delete('/api/contacts/:id', removeItem);
app.post('/api/contacts/delete', deleteSelected);

app.use(errorHandler);

var dao = new Dao();

dao.connect(url).then(() => {

    app.listen(3000, () => console.log('Server is running on port 3000')
);
}).catch(err => {
    console.log(err);
})

app.use(express.static('./'));

function getAllItems(request, response, next) {
    dao.findAll().then(contacts => response.json(contacts)
).catch(next);
}

function addItem(request, response, next) {
    var item = request.body;
    var contact = {
        "name": item.name,
        "phone": item.phone,
        "selected": false
    }
    dao.insert(contact).then(() => response.end()
).catch(next);
}

function updateItem(request,response,next){
    var id = request.params.id;
    var contact = request.body;
    var update = {
        "name": contact.name,
        "phone": contact.phone
    }
    dao.update(id, update).then(() => response.end()
).catch(next);
}

function removeItem(request, response, next){

    var id = request.params.id;
   //response.end(id);
   dao.remove(id).then((result) => response.json(result)
).catch(next);
}

function getItem(request, response, next){
    var id = request.params.id;
    dao.findById(id).then((result) => response.json(result)
).catch(next);
}
function deleteSelected(request, response, next){

    var ids = request.body;
    dao.removeMultiple(ids).then((result) => response.json(result)
).catch(next);

}

function errorHandler(error, request, response, next) {
    response.status(500).json({error: error.toString()});
}
