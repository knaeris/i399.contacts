var path = require('path');

module.exports = {

  entry: {
    app: './js/app.ts'
  },

  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/build/',
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'awesome-typescript-loader'
      }
    ],
    exprContextCritical: false
  },

  resolve: {
    extensions: ['.js', '.ts']
  }

};
