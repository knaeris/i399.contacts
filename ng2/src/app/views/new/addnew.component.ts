import {Component, OnInit} from '@angular/core';
import {ContactService} from "../../services/contact.service";
import {Contact} from "../../objectclasses/contact";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
    selector: 'app-addnew',
    templateUrl: './addnew.component.html',
    styleUrls: ['./addnew.component.css']
})
export class AddnewComponent implements OnInit {

    public contact: Contact = new Contact("", "");
    public id: any;

    constructor(private contactService: ContactService, private router: Router, public route: ActivatedRoute) {

    }

    ngOnInit() {
        this.route.params
            .subscribe(params => {
                this.id = params['id'];
            });

        this.getIdContact(this.id);

    }

    getIdContact(idGet) {
        if(!idGet) return;
        this.contactService.getById(idGet)
            .then( result => this.contact = result);
    }

    /*async getIdContact(idGet) {
        if(!idGet) return;
        this.contact = await this.contactService.getById(idGet)

    }*/

    saveContact() {
        if (this.id)
            this.contactService.edit(this.id, this.contact)
                .then(() => this.router.navigateByUrl("/search"))
        else
            this.contactService.insertContact(this.contact)
                .then(() => this.router.navigateByUrl("/search"))
    }


}