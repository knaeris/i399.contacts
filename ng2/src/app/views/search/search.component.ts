import {Component, OnInit} from '@angular/core';
import {ContactService} from "../../services/contact.service";
import {Contact} from "../../objectclasses/contact";

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

    public ids: any[] = [];
    public contacts: Contact[];
    public searchText: string = "";

    constructor(private contactService: ContactService) {
    }

    initContactList() {
        this.contactService.getContacts()
            .then(result => this.contacts = result);
    }

    ngOnInit() {
        this.initContactList()
    }

    selectedItems(id, selected) {

        if (selected) {
            this.ids.push(id);
        }
        let index = this.ids.indexOf(id);
        if (!selected && index > -1) {
            this.ids.splice(index, 1);
        }

    }


    removeContact(id) {
        this.contactService.deleteContact(id)
            .then(() => this.initContactList());
    }

    removeSelected() {
        this.contactService.deleteMany(this.ids)
            .then(() => this.initContactList());
    }



}
