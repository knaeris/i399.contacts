export class Contact {

    public contactName: string = "";
    public contactPhone: string = "";
    public selected: boolean = false;

    constructor(contactName: string, contactPhone:string) {
        this.contactName = contactName;
        this.contactPhone = contactPhone;
    }
}
