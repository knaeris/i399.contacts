import { Pipe, PipeTransform } from '@angular/core';
import {Contact} from "./objectclasses/contact";

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(contacts: Contact[], searchText: string): any[] {
        if(!contacts) return [];
        if(!searchText) return contacts;

        searchText = searchText.toLowerCase();

        return contacts.filter( it => {

            let hasBoth: boolean = it.contactName != undefined && it.contactPhone != undefined;
            let hasNoName: boolean = it.contactName == undefined && it.contactPhone != undefined;
            let hasNoNr:boolean = it.contactName != undefined && it.contactPhone == undefined;

            let name: string = it.contactName;
            let phone: string = it.contactPhone;

            if(hasBoth){
                return name.toLowerCase().includes(searchText) || phone.toLowerCase().includes(searchText);
            }

            if(hasNoName) {
                return phone.toLowerCase().includes(searchText);
            }

            if(hasNoNr) {
                return name.toLowerCase().includes(searchText);
            }


        });
    }
}
