import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchComponent} from "./views/search/search.component";
import {AddnewComponent} from "./views/new/addnew.component";

const routes: Routes = [
    { path: "search", component: SearchComponent },
    { path: "new", component: AddnewComponent },
    { path: "details/:id", component: AddnewComponent },
    { path: "", redirectTo: "search", pathMatch:"full"}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
