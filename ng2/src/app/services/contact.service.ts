import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Contact} from "../objectclasses/contact";

@Injectable({
    providedIn: 'root'
})
export class ContactService {

    constructor(private http: HttpClient) {
    }

    //Get All service
    public getContacts(): Promise<Contact[]> {
        return this.http.get("./contacts")
            .toPromise()
            .then(result => <Contact[]> result);
    }
    //get by id service
    public getById(id: any): Promise<Contact> {
        return this.http.get("./contacts/" + id)
            .toPromise()
            .then(result => <Contact> result);
    }

    public insertContact(contact: Contact): Promise<void> {
        return this.http.post("./contacts", contact)
            .toPromise()
            .then(() => <void> null);
    }

    public deleteContact(id: any): Promise<void> {
        return this.http.delete("./contacts/" + id)
            .toPromise()
            .then(() => <void> null);
    }

    public deleteMany(idNrs: any[]): Promise<void> {
        return this.http.post("./contacts/delete", idNrs)
            .toPromise()
            .then(() => <void> null);
    }

    public edit(id: any, contact: Contact): Promise<void> {
        return this.http.put("./contacts/" + id, contact)
            .toPromise()
            .then(() => <void> null);
    }

}
